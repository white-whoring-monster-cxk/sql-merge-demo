package com.zl.sqlmerge;

import com.zl.sqlmerge.service.testService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@EnableAutoConfiguration
@Slf4j
public class SQLT {

    @Autowired
    private testService service;

    private static final int Max = 10000;

    private CountDownLatch countDownLatch = new CountDownLatch(Max);


    @Test
    public void Test01() throws InterruptedException, IOException, ExecutionException {
        for (int i = 0; i < Max; i++) {
            final  String id = String.valueOf(i+100);
            new Thread(()->{
                try {
                    countDownLatch.countDown();;
                    countDownLatch.await();
                    String s = service.test01(id);
                    //System.out.println("查询结果"+s);
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }).start();
        }
        log.info("线程准备完成====");
//        FutureTask<Object> objectFutureTask = new FutureTask<>(new Callable<Object>() {
//            @Override
//            public Object call() throws Exception {
//                return null;
//            }
//        });
//        new Thread(objectFutureTask).start();
//        objectFutureTask.get();

        new CompletableFuture<Object>().get();

    }

}


