package com.zl.sqlmerge.entity;


import java.util.concurrent.CompletableFuture;

/**
 * 请求封装
 * @author wuze
 */
public class request {

    private String id;
    private String uuid;
    private CompletableFuture<String> future;

    public request(String id, String uuid, CompletableFuture<String> future) {
        this.id = id;
        this.uuid = uuid;
        this.future = future;
    }

    public CompletableFuture<String> getFuture() {
        return future;
    }

    public void setFuture(CompletableFuture<String> future) {
        this.future = future;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public String toString() {
        return "request{" +
                "id='" + id + '\'' +
                ", uuid='" + uuid + '\'' +
                ", future=" + future +
                '}';
    }
}
