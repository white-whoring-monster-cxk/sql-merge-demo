# SQL合并demo

## 介绍
使用sql合并思想，解决大量并发请求攻击到mysql，将10000次请求减少为10次左右

![img.png](./images/img.png)

###  核心思想

假设有一个根据id查询数据的请求

首先将id封装成一个request，存放入队列中

![img_4.png](./images/img_4.png)

调用CompletableFuture的get方法阻塞线程

![img_1.png](./images/img_1.png)

后台新建一个定时线程池scheduledExecutorService每隔20ms扫描队列

![img_2.png](./images/img_2.png)

将id封装成一个list集合，调用后台list集合接口批量查询数据库

![img_3.png](./images/img_3.png)

最后根据返回的唯一id找到对应的request，调用其中CompletableFuture的complete方法


##  结束
