package com.zl.sqlmerge.controller;


import com.zl.sqlmerge.service.testService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


import java.util.concurrent.*;

@RestController
public class testController {

    @Autowired
    private testService service;

    @GetMapping("getid/{id}")
    public String test01(@PathVariable("id") String id) throws ExecutionException, InterruptedException {
        return service.test01(id);
    }

}
